#!/bin/bash
set -e

sudo modprobe nbd max_part=10

tar --exclude-vcs -z -c -v -f copy-to-filesystem.tar.gz copy-to-filesystem/

touch authorized_keys

sudo DOTNET_CLI_TELEMETRY_OPTOUT=true ACCEPT_EULA=Y imageconf/bootstrap-vz/bootstrap-vz --debug imageconf/vmtemplate.yml
